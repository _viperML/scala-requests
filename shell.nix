let
  pkgs = import <nixpkgs> {};
in
  with pkgs;
    mkShell.override {
      stdenv = stdenvNoCC;
    } {
      packages = [
        sbt
        scala
        coursier
      ];
    }

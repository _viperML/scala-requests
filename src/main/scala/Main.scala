import cats.effect.{IO, IOApp}
import sttp.client3._
import io.circe._
import io.circe.parser._

val backend = asynchttpclient.cats.AsyncHttpClientCatsBackend[IO]()

val httpbin_response = for {
  b <- backend
  request = basicRequest.get(uri"https://httpbin.org/get")
  response <- request.send(b)
} yield response

def parseResponse(
    response: Response[Either[String, String]]
): Either[ParsingFailure, Json] = response.body match
  case Left(failure_msg) => Left(ParsingFailure(failure_msg, Throwable()))
  case Right(body)       => parse(body)

object Main extends IOApp.Simple {
  val run = for {
    _ <- IO.println("Hello world")

    response <- httpbin_response

    response_parsed = parseResponse(response)

    toPrint <- response_parsed match
      case Left(failure) => IO.println("Parsing failed")
      case Right(json) => IO.println(json)

  } yield ()
}
